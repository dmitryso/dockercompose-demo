<h1>This is a PHP script from App1</h1>
<h3>ENV</h3>
<pre>
  <?php var_export($_SERVER)?>
</pre>

<h3>Postgres connection</h3>
<pre>
  <?php
    $db_host = getenv('POSTGRES_PORT_5432_TCP_ADDR');
    $db_port = getenv('POSTGRES_PORT_5432_TCP_PORT');
    $db_conn = pg_connect("host=$db_host port=$db_port password=123456789 user=postgres dbname=postgres");

    $db_query = "select count(*) from information_schema.tables;";
    $db_result = pg_query($db_conn, $db_query);

    while ($row = pg_fetch_row($db_result)) {
      echo "$db_query = $row[0]";
    }
  ?>
</pre>
