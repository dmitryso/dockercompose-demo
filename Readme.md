So here is a docker-compose.yml setup that does the same thing as the previous vagrant box.
This repo contains a few app subdirs, and serves them using one docker-compose.yml, but you can leave only
one app and run other using another docker-compose.yml's. There is a another section on how to do it.

This setup expects that each app has it's own Dockerfile which copies the app code into the container. For development
purposes the code in the container is shadowed by the mounted directory with the current code version.

The postgres Dockerfile can be extracted to a new image, as it is not expected to be different between projects.
But the nginx Dockerfile is needed as nginx container is rebuild using the current config on each deployment.

I haven't added any nginx config generator as anyway the production config will be more complicated than
any generated thing. There is a sample nginx config talking to a few node/php apps in the nginx/conf.d directory.
Anything you put in that dir will be loaded by the nginx. The only important thing is to reference the backends
in the correct way, e.g.

    upstream nodeapp1-upstream { server dockercomposedemo_node_app1_1:3000; }
    upstream nodeapp2-upstream { server dockercomposedemo_node_app2_1:3000; }
    upstream phpapp1-upstream  { server dockercomposedemo_php_app1_1:9000; }
    upstream phpapp2-upstream  { server dockercomposedemo_php_app2_1:9000; }

"dockercomposedemo" is the name of the folder containing the docker-compose.yml, "node_app1" is the name of the app,
and "1" is the number of the container (you can launch many of them), the port is fixed.

So to start just run:

    docker-compose up -d

And there should be a bunch of services:

    docker-compose ps

    dockercomposedemo_nginx_1           nginx -g daemon off;             Up      0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp
    dockercomposedemo_node_app1_1       node .                           Up      3000/tcp
    dockercomposedemo_node_app2_1       node .                           Up      3000/tcp
    dockercomposedemo_php_app1_1        php-fpm                          Up      9000/tcp
    dockercomposedemo_php_app2_1        php-fpm                          Up      9000/tcp
    dockercomposedemo_beanstalkd_1      beanstalkd -p 11300              Up      11300/tcp
    dockercomposedemo_elasticsearch_1   /docker-entrypoint.sh elas ...   Up      9200/tcp, 9300/tcp
    dockercomposedemo_postgres_1        /docker-entrypoint.sh postgres   Up      0.0.0.0:5432->5432/tcp
    dockercomposedemo_redis_1           /entrypoint.sh redis-server      Up      6379/tcp

And you should be able to hit those URLs:

    open http://`docker-machine ip default`/node_app1
    open http://`docker-machine ip default`/node_app2
    open http://`docker-machine ip default`/php_app1/test.php
    open http://`docker-machine ip default`/php_app2/test.php


## Running multiple docker-compose.yml's together

In a case if you want to run a few apps, each  with different docker-compose.yml at the same time —
there are few ways to achieve it.

1) Just put it all into one docker-compose.yml, like it is done here.

2) You can actually link services from one docker-compose.yml to another one, so you can run a few docker-compose clusters
with different apps and backend services (but without the nginx) and one docker-compose with the nginx only. And that shared
nginx will connect to all the backend apps.

There is a sample file named *docker-nginx.yml* which defrines an nginx connecting to the main docker-compose.yml.
So running something like this will launch two docker-compose groups and the second one will reference
services from the first one. Obviously that you can reference services from other docker compose groups as well.

      docker-compose up -d
      docker-compose -f docker-nginx.yml -p shared_nginx up -d

In this sample the Nginx runs on port 5080, but if you disable the nginx from the docker-compose.yml then you can run it
on the default port.

3) Another option, probably the best one, is to run each app on a new VM, so everything related to a single app
will run in a separate VM. Running something like that will make it work.

      docker-machine create -d virtualbox another
      eval "$(docker-machine env another)"
      docker-compose up -d

Now if you run something like this:

      open http://`docker-machine ip default`/php_app1/test.php
      open http://`docker-machine ip another`/php_app1/test.php

You should see that you have 2 copies of those docker containers running at the same time. Obviously that instead of
running the same stuff you can run different apps this way.


## Deployment

Deployment to digital ocean works almost the same way as deployment to another virtual box VM.
Just create a docker host on a new DO droplet (I assume that you have you DO access token in a `DIGITAL_OCEAN_TOKEN` environment variable).

    docker-machine create -d digitalocean --digitalocean-access-token=$DIGITAL_OCEAN_TOKEN --digitalocean-size 2GB staging

Eval the droplet settings into the current shell:

    eval "$(docker-machine env staging)"

And now just launch all the services:

    docker-compose -f docker-production.yml up -d

Now you should have it all running at the droplet IP (get one using `docker-machine ip staging`).

As you see the production setup uses an alternative docker-production.yml.
The difference between docker-compose.yml and docker-production.yml is that:

* All the local volumes with the code are not mounted, instead the code from inside of the containers is used.
* All the ports, except 80/443 are closed from the outside.
The docker-compose.yml has the postgres open so you can connect using some Mac GUI, also you can open any other
services, just add an explicit port mapping to the ports section.
* Also the postgres data directory is mounted to the droplet, so data will not be lost
if you remove the postgres container accidentially. Same thing can be done for other backend services, while it's not as
critical for them. Ideally those linked directories should be stored outside of the instance on some external storage,
but that's not an option for the DO.

Docker-Compose has an ability to inherit/extend one config from another one so you may want the
development version to inherit from the production one, but the extending syntax is quite verbose so
I'm not sure that it's much better that copy-pasting.

And, just to mention, you should prepend that "-f docker-production.yml" to all production docker-compose commands, otherwise
you'll get weird errors.


## Creating databases

First of all there is a way to create a new database directly using the official postgres image: just set the name of
the database to create in the POSTGRES_DB variable in docker-compose.yml. The limitation is that
you can set only one database and it will be created only on the very first postgres start.

So in the custom postgres image I've added a script to add more databases at any time.
Just set the database names into the DATABASES variable, it should be a space separated list "db1 db2 db3". And you
should not use the quotes when setting the variable in the docker-compose.yml (otherwise it will be treated as a one database
name with spaces inside). After that, assuming that the postgres is already up, run something like:

    docker exec dockercomposedemo_postgres_1 /create-databases.sh

You can see existing databases using:

    docker exec dockercomposedemo_postgres_1 psql --username postgres -c "SELECT datname FROM pg_database WHERE datistemplate = false"

## Using the php-tools image

Generally you can use it using docker directly:

    docker run -v /Users/Dima/Code/Active/docker-demo/data/phpapp_with_tests:/app --rm -ti darh/php-essentials phpcs --standard=Zend /app/src/Currency.php

    docker run -v /Users/Dima/Code/Active/docker-demo/data/phpapp_with_tests:/app --rm -ti darh/php-essentials phpunit --bootstrap /app/src/autoload.php /app/tests

Or add it to the docker-compose.yml

    phptools:
      image: darh/php-essentials
      volumes:
        - "/Users/Dima/Code/Active/docker-demo/data/phpapp_with_tests:/app"

And then:

    docker-compose run --rm phptools phpcs --standard=Zend /app/src/Currency.php

    docker-compose run --rm phptools phpunit --bootstrap /app/src/autoload.php /app/tests

(The `phpapp_with_tests` is a part of the previous repo.)
